# VideoID PHP SDK

### Installation
In order to install the PHP SDK you have to update your ``composer.json`` file with the following code:

```
#!json
{
    "require": {
        "videoid/php-sdk": "^1.2.2"
    },
    "repositories": [
        {
            "type": "vcs",
            "url":  "https://bitbucket.org/adfamilies/videoid_php_sdk.git"
        }
    ]
}
```

After doing that you are all set to use videoID’s SDK.

### How to use

To use the SDK follow these 3 steps:

## (1st) GET SDK Tokens

Your PHP SDK token keys are provided in the email.

## (2nd) Initialize SDK

```
#!php

$sdk = new VideoID\SDK\VideoIDSDK("{TOKEN_KEY}", "{TOKEN_SECRET}", "{ID_SECRET}");
```

Note: Pass your tokens into the constructor.

## (3rd) Start using

After you have constructed the SDK object you can start a query. To start a query it is required to pass a ``SplFileInfo`` object. So, let's fetch it:

```
#!php

$file = new \SplFileInfo("segment.mp4");
```

When we have the file we can start the query:

```
#!php

$result = $sdk->query($file);
```

Now if everything goes smooth, the method will return a result array.

```
[

  [
     id => null,
     name => "No match",
     intervals => [
        [2,4],
        [6,8]
     ],
     percentage => 20
  ],
  [
     id => "2-4",
     name => "Video B",
     intervals => [
        [0,2],
        [4,6],
        [8,14]
     ],
     percentage => 50
  ]
  [
     id => "4-8",
     name => "Video D",
     intervals => [
        [14,20]
     ],
     percentage => 30
  ]
]
```

The result array is a collection of reference videos matched and/or a "no match element", which consist of:

- ``id``: the id of the matched reference video

- ``name``: the name of the matched reference video

- ``intervals``: the time intervals (in seconds) of the query video that gave a match

- ``percentage``: total percentage of the query video that gave a match (0 - 100) 


When something goes wrong an exception will be thrown which can be:

- ``VideoID\SDK\Exceptions\LimitExceededException``
If the app has exceeded its duration (seconds) of reference videos capacity.

- ``VideoID\SDK\Exceptions\InvalidCredentialsException``
If the app credentials specific for the PHP SDK you provided are incorrect.

- ``VideoID\SDK\Exceptions\InternalErrorException``
If there was an internal error with our PHP SDK (Probably our bad! :X)

### Documentation

## VideoID\SDK\VideoIDSDK

-- ``query($file)``

Receives a ``SplFileInfo`` object and proceeds with the query process, it returns a result array.

-- ``asyncUploadReference($label, $file)``

Receives a name ``label`` and a ``SplFileInfo`` object then uploads the reference to be processed. This method is asynchronous after the reference video has been uploaded to the system: it returns the reference id, an (int).

-- ``getReference($referenceID)``

Receives a ``referenceID`` and fetches the state of that reference: it returns a ``Reference`` object.

## VideoID\SDK\Reference

-- ``getID()``

Get the video reference id, returns a (String). 

-- ``getName()``

Get the video reference label, returns a (String).

-- ``getDuration()``

Get the video duration in seconds, returns an (int).

-- ``isProcessed()``

If the reference video is fully processed it returns true otherwise false, returns a (boolean).

### Process Reference Example

```
#!php

<?php 

require('vendor/autoload.php');

$key = "";
$secret = "";
$idSecret = "";

$sdk = new VideoID\SDK\VideoIDSDK($key, $secret, $idSecret);

try {

    // Let's upload our video to be processed.
    $file = new \SplFileInfo("/reference_video.mp4"); // Create the file
    $referenceID = $sdk->asyncUploadReference("Video", $file); // Upload the reference video
    print("ReferenceID: " . $referenceID . "\n");

    // To get more information about the reference video at any time, call getReference.
    $reference = $sdk->getReference($referenceID);
    print("Reference State: " . ($reference->isProcessed() ? 'Done' : 'Processing') . "\n");
    print("Reference Name: " . $reference->getName() . "\n");
    print("Reference Duration: " . $reference->getDuration() . "\n");

} catch (VideoID\SDK\Exceptions\LimitExceededException $e) {
    var_dump($e->getMessage());
    die("Limit exceeded error...\n");
} catch (VideoID\SDK\Exceptions\InvalidCredentialsException $e) {
    var_dump($e->getMessage());
    die("Invalid Credentials error...\n");
} catch (VideoID\SDK\Exceptions\InternalErrorException $e) {
    var_dump($e->getMessage());
    die("SDK internal error...\n");
}
```

Note: To test the query videos make sure all the reference videos have been processed.

### Query Example

```
#!php

<?php 

require('vendor/autoload.php');

$key = "";
$secret = "";
$idSecret = "";

$sdk = new VideoID\SDK\VideoIDSDK($key, $secret, $idSecret);

try {

    // After our reference has been processed we can make a query and see if it matches.
    $file = new \SplFileInfo("/query_video.mp4"); // Create the file
    $result = $sdk->query($file); // Query the test video
    print("Result: " . var_export($result, true) . "\n");

} catch (VideoID\SDK\Exceptions\LimitExceededException $e) {
    var_dump($e->getMessage());
    die("Limit exceeded error...\n");
} catch (VideoID\SDK\Exceptions\InvalidCredentialsException $e) {
    var_dump($e->getMessage());
    die("Invalid Credentials error...\n");
} catch (VideoID\SDK\Exceptions\InternalErrorException $e) {
    var_dump($e->getMessage());
    die("SDK internal error...\n");
}
```
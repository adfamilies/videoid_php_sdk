<?php

use VideoID\SDK\VideoIDSDK;

/**
 * @runTestsInSeparateProcesses
 * @preserveGlobalState disabled
 */
class VideoIDSDKTest extends TestCase {
	

	/** @test */
	public function it_can_be_instantiated() {
		$sdk = new VideoIDSDK('valid-key', 'valid-secret', 'valid-auth');

		$this->assertNotNull($sdk);
	}

	/** @test */
	public function it_can_upload_a_reference_video() {
		$sdk = new VideoIDSDK('valid-key', 'valid-secret', 'valid-auth');

		$TotalDurationRequestMock = Mockery::mock('overload:VideoID\SDK\Request\TotalDurationRequest');
		$TotalDurationRequestMock->shouldReceive('send')->andReturn(400);

		$CreateMediaRequestMock = Mockery::mock('overload:VideoID\SDK\Request\CreateMediaRequest');
		$CreateMediaRequestMock->shouldReceive('send')->andReturn(21);

		$CreateResourceRequestMock = Mockery::mock('overload:VideoID\SDK\Request\CreateResourceRequest');
		$CreateResourceRequestMock->shouldReceive('send')->andReturn(56);

		$ViewMediaRequestMock = Mockery::mock('overload:VideoID\SDK\Request\ViewMediaRequest');
		$ViewMediaRequestMock->shouldReceive('send')->andReturn(json_decode("{ \"id\": 21, \"name\": \"media test\" }"));

		$ViewResourceRequestMock = Mockery::mock('overload:VideoID\SDK\Request\ViewResourceRequest');
		$ViewResourceRequestMock->shouldReceive('send')->andReturn(json_decode("{ \"id\": 56, \"duration\": 84, \"process_status\": \"DATA_SAVED\" }"));

		$referenceId = $sdk->uploadReference("test", new FakeFile("test.file"));

		$this->assertEquals("21-56", $referenceId);
	}

	/** @test */
	public function it_can_async_upload_a_reference_video() {
		$sdk = new VideoIDSDK('valid-key', 'valid-secret', 'valid-auth');

		$TotalDurationRequestMock = Mockery::mock('overload:VideoID\SDK\Request\TotalDurationRequest');
		$TotalDurationRequestMock->shouldReceive('send')->andReturn(400);

		$CreateMediaRequestMock = Mockery::mock('overload:VideoID\SDK\Request\CreateMediaRequest');
		$CreateMediaRequestMock->shouldReceive('send')->andReturn(21);

		$CreateResourceRequestMock = Mockery::mock('overload:VideoID\SDK\Request\CreateResourceRequest');
		$CreateResourceRequestMock->shouldReceive('send')->andReturn(56);

		$referenceId = $sdk->asyncUploadReference("test", new FakeFile("test.file"));

		$this->assertEquals("21-56", $referenceId);
	}

	/** @test */
	public function it_can_get_a_reference_video_by_id() {
		$sdk = new VideoIDSDK('valid-key', 'valid-secret', 'valid-auth');

		$ViewMediaRequestMock = Mockery::mock('overload:VideoID\SDK\Request\ViewMediaRequest');
		$ViewMediaRequestMock->shouldReceive('send')->andReturn(json_decode("{ \"id\": 21, \"name\": \"media test\" }"));

		$ViewResourceRequestMock = Mockery::mock('overload:VideoID\SDK\Request\ViewResourceRequest');
		$ViewResourceRequestMock->shouldReceive('send')->andReturn(json_decode("{ \"id\": 56, \"duration\": 84, \"process_status\": \"DATA_SAVED\" }"));

		$reference = $sdk->getReference("21-56");

		$this->assertNotNull($reference);
		$this->assertEquals("21-56", $reference->getId());
		$this->assertEquals("media test", $reference->getName());
		$this->assertEquals(84, $reference->getDuration());
		$this->assertTrue($reference->isProcessed());
	}

	/** @test */
	public function it_can_query_a_video() {
		$sdk = new VideoIDSDK('valid-key', 'valid-secret', 'valid-auth');

		$TotalDurationRequestMock = Mockery::mock('overload:VideoID\SDK\Request\VideoIDMatchRequest');
		$TotalDurationRequestMock->shouldReceive('send')->andReturn(243);

		$CreateMediaRequestMock = Mockery::mock('overload:VideoID\SDK\Request\VideoIDResultRequest');
		$CreateMediaRequestMock->shouldReceive('send')->andReturn(json_decode(file_get_contents(__DIR__ . '/../responses/video_id_result.json')));

		$results = $sdk->query(new FakeFile("test.file"));

		$this->assertEquals([
			0 => [ 
				'id' => null,
			    'name' => 'No Match',
			    'intervals' => [
			    	[0,2],[12,14],[16,18],[20,22],[28,32],[36,40],[44,46],[62,64],[72,74],[76,78],[96,98],[100,110],
			    	[112,114],[130,134],[136,138],[142,152],[168,170],[176,180],[210,228],[230,232],[234,236],[238,240],
			    	[246,248],[262,266],[270,276],[284,288],[294,296],[310,314],[320,322],[326,332],[356,360],[408,412],
			    	[420,428],[440,442],[450,452],[462,466],[478,480]
			    ],
				'percentage' => 29,
			],
			67 => [
				'id' => '67-68',
				'name' => 'Video-2_15',
				'intervals' => [
					[2,4],[8,10],[14,16],[18,20],[24,26]
				],
				'percentage' => 2,
			],
				75 => [
				'id' => '75-76',
				'name' => 'Video-1_21',
				'intervals' => [
					[4,8],[10,12],[22,24],[26,28]
				],
				'percentage' => 2,
			],
			69 => [
			  'id' => '69-69',
			  'name' => 'Video-4_20',
			  'intervals' => [
			    [32,36],[40,44],[46,62],
			  ],
			  'percentage' => 5,
			],
			79 => [
			    'id' => '79-79',
			    'name' => 'Video-4_23',
			    'intervals' => [
			      [64,72],[74,76],[78,96],[98,100],
			    ],
			    'percentage' => 6,
			],
			851 => [
			    'id' => '851-824',
			    'name' => 'Video-24_6',
			    'intervals' => [
			      [110,112],[116,118],[126,128],
			    ],
			    'percentage' => 1,
			],
			127 =>
			  [
			    'id' => '127-127',
			    'name' => 'Video-5_2',
			    'intervals' => [
			      [114,116],[118,126],[128,130],[134,136],[138,142],
			    ],
			    'percentage' => 4,
			  ],
			144 => [
			    'id' => '144-144',
			    'name' => 'Video-5_7',
			    'intervals' => [
			      [152,156],[158,162],[164,168],[170,172],[174,176],
			    ],
			    'percentage' => 3,
			],
			880 => [
			    'id' => '880-853',
			    'name' => 'Video-23_27',
			    'intervals' => [
			      [156,158],[162,164],[172,174],[180,184],
			    ],
			    'percentage' => 2,
			],
			667 => [
			    'id' => '667-659',
			    'name' => 'Video-18_16',
			    'intervals' => [
			      [184,188],[206,210],
			    ],
			    'percentage' => 2,
			],
			188 => [
			    'id' => '188-189',
			    'name' => 'Video-5_18',
			    'intervals' => [
			      [188,206],
			    ],
			    'percentage' => 4,
			],
			205 => [
			    'id' => '205-205',
			    'name' => 'Video-5_23',
			    'intervals' => [
			      [228,230],[232,234],[236,238],[240,246],[248,262],
			    ],
			    'percentage' => 5,
			],
			139 => [
			    'id' => '139-139',
			    'name' => 'Video-6_4',
			    'intervals' => [
			      [266,270],[276,284],[288,294],[296,310],
			    ],
			    'percentage' => 7,
			],
			265 => [
			    'id' => '265-265',
			    'name' => 'Video-6_39',
			    'intervals' => [
			      [314,320],[322,326],[332,352],
			    ],
			    'percentage' => 6,
			],
			134 => [
			    'id' => '134-135',
			    'name' => 'Video-7_2',
			    'intervals' => [
			      [352,356],[360,408],[412,420],
			    ],
			    'percentage' => 12,
			],
			195 => [
			    'id' => '195-195',
			    'name' => 'Video-7_15',
			    'intervals' => [
			      [428,440],[442,448],[452,462],[466,478],[480,482]
			    ],
			    'percentage' => 9,
			],
			241 => [
			    'id' => '241-241',
			    'name' => 'Video-7_29',
			    'intervals' => [
			      [448,450]
			    ],
			    'percentage' => 0,
			],
		], $results);
	}

}

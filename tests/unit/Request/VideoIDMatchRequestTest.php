<?php

use VideoID\SDK\Request\VideoIDMatchRequest;

class VideoIDMatchRequestTest extends TestCase {
	
	/** @test */
	public function it_gets_the_result_of_videoid_match_by_id()
	{
		$request = new VideoIDMatchRequest('valid-auth');
		$history = [];
		$this->hijackRequest($request, [
			new GuzzleHttp\Psr7\Response(200, $headers = [], $body = file_get_contents(__DIR__ . '/../../responses/video_id_match.json'))
		], $history);

		$result = $request->send(new FakeFile("test.file"));
		$request = $history[0]['request'];

		$this->assertRequestIsVideoIDSigned($request);
		$this->assertEquals('POST', $request->getMethod());
		$this->assertEquals('/matching', $request->getUri()->getPath());
		$this->assertEquals(243, $result);
	}

}
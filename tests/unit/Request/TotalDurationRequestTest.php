<?php

use VideoID\SDK\Request\TotalDurationRequest;

class TotalDurationRequestTest extends TestCase {
	
	/** @test */
	public function it_returns_the_total_duration_of_the_system()
	{
		$request = new TotalDurationRequest('valid-key', 'valid-secret');
		$history = [];
		$this->hijackRequest($request, [
			new GuzzleHttp\Psr7\Response(200, $headers = [], $body = "4780")
		], $history);

		$duration = $request->send();
		$request = $history[0]['request'];

		$this->assertRequestIsBoomSigned($request);
		$this->assertEquals('GET', $request->getMethod());
		$this->assertEquals('/v2.0/app/total-duration', $request->getUri()->getPath());
		$this->assertEquals(4780, $duration);
	}

}
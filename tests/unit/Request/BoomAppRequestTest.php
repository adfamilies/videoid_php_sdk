<?php

use VideoID\SDK\Request\BoomAppRequest;
use VideoID\SDK\Exceptions\InternalErrorException;
use VideoID\SDK\Exceptions\InvalidCredentialsException;

class BoomAppRequestTest extends TestCase {

	/** @test */
	public function it_throws_an_exception_for_invalid_key()
	{
		$request = new FakeRequest('invalid-key', 'invalid-secret');
		$history = [];
		$this->hijackRequest($request, [
			new GuzzleHttp\Psr7\Response(400, $headers = [], $body = "{ \"error\": { \"reason\": \"invalidApiId\", \"message\": \"Invalid credentials.\" } }")
		], $history);

		try {
			$request->send();
		} catch(InvalidCredentialsException $e) {
			return;
		}

		$this->fail("Expected to throw an InvalidCredentialsException...");
	}

	/** @test */
	public function it_throws_an_exception_for_invalid_credentials()
	{
		$request = new FakeRequest('invalid-key', 'invalid-secret');
		$history = [];
		$this->hijackRequest($request, [
			new GuzzleHttp\Psr7\Response(403, $headers = [], $body = "{ \"error\": { \"reason\": \"forbidden\", \"message\": \"Invalid credentials.\" } }")
		], $history);

		try {
			$request->send();
		} catch(InvalidCredentialsException $e) {
			return;
		}

		$this->fail("Expected to throw an InvalidCredentialsException...");
	}

	/** @test */
	public function it_throws_an_exception_for_empty_response()
	{
		$request = new FakeRequest('invalid-key', 'invalid-secret');
		$history = [];
		$this->hijackRequest($request, [
			new GuzzleHttp\Psr7\Response(200, $headers = [], $body = "")
		], $history);

		try {
			$request->send();
		} catch(InternalErrorException $e) {
			return;
		}

		$this->fail("Expected to throw an InternalErrorException...");
	}

	/** @test */
	public function it_throws_an_exception_for_bad_request()
	{
		$request = new FakeRequest('invalid-key', 'invalid-secret');
		$history = [];
		$this->hijackRequest($request, [
			new GuzzleHttp\Psr7\Response(400, $headers = [], $body = "{ \"error\": { \"reason\": \"badRequest\", \"message\": \"Bad request\" } }")
		], $history);

		try {
			$request->send();
		} catch(InternalErrorException $e) {
			return;
		}

		$this->fail("Expected to throw an InternalErrorException...");
	}

	/** @test */
	public function it_throws_an_exception_for_communication_problems()
	{
		$request = new FakeRequest('invalid-key', 'invalid-secret');
		$history = [];
		$this->hijackRequest($request, [
			new GuzzleHttp\Exception\RequestException("Error Communicating with Server", new GuzzleHttp\Psr7\Request('GET', 'test'))
		], $history);

		try {
			$request->send();
		} catch(InternalErrorException $e) {
			return;
		}

		$this->fail("Expected to throw an InternalErrorException...");
	}

	

}

class FakeRequest extends BoomAppRequest
{
    
    public function send()
    {
        $httpClient = new GuzzleHttp\Client(['handler' => $this->stack, 'connect_timeout' => 30, ]);
        $options = [
            'headers' => [
                'x-boom-date'           => gmdate('Ymd\THis\Z', time()),
                'x-boom-sdk-platform'   => 'PHP',
                'x-boom-content-sha256' => 'c633444c758c27f2518c5e27096ffb356790513d6b57792a71ad6a78a4a42c04'
            ],
        ];
        try {
            $response = $httpClient->request('TEST', $this->apiBaseUrl . $this->apiVersion .'/fake/test', $options);
        } catch (GuzzleHttp\Exception\RequestException $e) {
            $this->handleRequestException($e);
        } catch (\Exception $e) {
            throw new InternalErrorException($e->getMessage());
        }
        $this->handleResponseException($response);
        $header = 'HTTP/1.1 ' . $response->getStatusCode() . ' ' . $response->getReasonPhrase();
        $headers = $response->getHeaders();
        array_unshift($headers, $header);
        try {
            $body = json_decode($response->getBody());
            return $body;
        } catch (\Exception $e) {
            throw new InternalErrorException($response->getBody());
        }
    }
}
<?php

use VideoID\SDK\Request\ViewResourceRequest;

class ViewResourceRequestTest extends TestCase {
	
	/** @test */
	public function it_gets_the_resource_by_media_and_resource_id()
	{
		$request = new ViewResourceRequest('valid-key', 'valid-secret');
		$history = [];
		$this->hijackRequest($request, [
			new GuzzleHttp\Psr7\Response(200, $headers = [], $body = "{ \"status\": \"OK\", \"payload\": { \"id\": 56, \"duration\": 84, \"process_status\": \"DATA_SAVED\" } }")
		], $history);

		$resource = $request->send(21, 34);
		$request = $history[0]['request'];

		$this->assertRequestIsBoomSigned($request);
		$this->assertEquals('GET', $request->getMethod());
		$this->assertEquals('/v2.0/media/21/resources/34', $request->getUri()->getPath());
		$this->assertNotNull($resource);
		$this->assertEquals(56, $resource->id);
		$this->assertEquals(84, $resource->duration);
		$this->assertEquals("DATA_SAVED", $resource->process_status);
	}

}
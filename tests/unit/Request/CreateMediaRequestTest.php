<?php

use VideoID\SDK\Request\CreateMediaRequest;

class CreateMediaRequestTest extends TestCase {
	
	/** @test */
	public function it_creates_a_media_with_name()
	{
		$request = new CreateMediaRequest('valid-key', 'valid-secret');
		$history = [];
		$this->hijackRequest($request, [
			new GuzzleHttp\Psr7\Response(200, $headers = [], $body = "{ \"status\": \"OK\", \"payload\": { \"id\": 21, \"name\": \"media test\" } }")
		], $history);

		$mediaId = $request->send("media test");
		$request = $history[0]['request'];

		$this->assertRequestIsBoomSigned($request);
		$this->assertEquals('POST', $request->getMethod());
		$this->assertEquals('/v2.0/media', $request->getUri()->getPath());
		$this->assertEquals(21, $mediaId);
	}

}
<?php

use VideoID\SDK\Request\CreateResourceRequest;

class CreateResourceRequestTest extends TestCase {
	
	/** @test */
	public function it_creates_the_resource_for_media_with_id_and_file()
	{
		$request = new CreateResourceRequest('valid-key', 'valid-secret');
		$history = [];
		$this->hijackRequest($request, [
			new GuzzleHttp\Psr7\Response(200, $headers = [], $body = "{ \"status\": \"OK\", \"payload\": { \"id\": 56, \"duration\": 84, \"process_status\": \"DATA_SAVED\" } }")
		], $history);

		$resourceId = $request->send(21, new FakeFile("test.file"));
		$request = $history[0]['request'];

		$this->assertRequestIsBoomSigned($request);
		$this->assertEquals('POST', $request->getMethod());
		$this->assertEquals('/v2.0/media/21/resources', $request->getUri()->getPath());
		$this->assertEquals(56, $resourceId);
	}

}

<?php

use VideoID\SDK\Request\ViewMediaRequest;

class ViewMediaRequestTest extends TestCase {
	
	/** @test */
	public function it_gets_the_media_by_id()
	{
		$request = new ViewMediaRequest('valid-key', 'valid-secret');
		$history = [];
		$this->hijackRequest($request, [
			new GuzzleHttp\Psr7\Response(200, $headers = [], $body = "{ \"status\": \"OK\", \"payload\": { \"id\": 21, \"name\": \"media test\" } }")
		], $history);

		$media = $request->send(21);
		$request = $history[0]['request'];

		$this->assertRequestIsBoomSigned($request);
		$this->assertEquals('GET', $request->getMethod());
		$this->assertEquals('/v2.0/media/21', $request->getUri()->getPath());
		$this->assertNotNull($media);
		$this->assertEquals(21, $media->id);
		$this->assertNotNull("media test", $media->name);
	}

}
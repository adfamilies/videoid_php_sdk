<?php

use VideoID\SDK\Request\VideoIDResultRequest;

class VideoIDResultRequestTest extends TestCase {
	
	/** @test */
	public function it_gets_the_result_of_videoid_match_by_id()
	{
		$request = new VideoIDResultRequest('valid-auth');
		$history = [];
		$this->hijackRequest($request, [
			new GuzzleHttp\Psr7\Response(200, $headers = [], $body = file_get_contents(__DIR__ . '/../../responses/video_id_result.json'))
		], $history);

		$result = $request->send(243);
		$request = $history[0]['request'];

		$this->assertRequestIsVideoIDSigned($request);
		$this->assertEquals('GET', $request->getMethod());
		$this->assertEquals('/matchings', $request->getUri()->getPath());
		$this->assertEquals('process_id=243', $request->getUri()->getQuery());
		$this->assertNotNull($result);
		$this->assertEquals('DONE', $result->status);
	}

}
<?php

use VideoID\SDK\Reference;

class ReferenceTest extends PHPUnit_Framework_TestCase {
	
	private function makeReference($processed = true) {
		$mediaState = "{ \"id\": 23, \"name\": \"reference test\" }";
		if ($processed) {
			$status = 'DATA_SAVED';
		} else {
			$status = 'SAVING_DATA';
		}
		$resourceState = "{ \"id\": 56, \"duration\": 84, \"process_status\": \"" . $status . "\" }";

		return new Reference(json_decode($mediaState), json_decode($resourceState));

	}

	/** @test */
	public function it_can_be_instantiated() {
		$reference = $this->makeReference();

		$this->assertNotNull($reference);
	}

	/** @test */
	public function it_can_retrieve_its_id() {
		$reference = $this->makeReference();

		$this->assertEquals('23-56', $reference->getID());
	}

	/** @test */
	public function it_can_retrieve_its_name() {
		$reference = $this->makeReference();

		$this->assertEquals('reference test', $reference->getName());
	}

	/** @test */
	public function it_can_retrieve_its_duration() {
		$reference = $this->makeReference();

		$this->assertEquals(84, $reference->getDuration());
	}

	/** @test */
	public function it_can_know_when_its_processed() {
		$processedRef = $this->makeReference($processed = true);
		$notProcessedRef = $this->makeReference($processed = false);

		$this->assertTrue($processedRef->isProcessed());
		$this->assertFalse($notProcessedRef->isProcessed());
	}
	
	/** @test */
	public function the_static_can_create_reference_ids() {
		$mediaId = 23;
		$resourceId = 24;

		$referenceId = Reference::createReferenceID($mediaId, $resourceId);

		$this->assertEquals('23-24', $referenceId);
	}

	/** @test */
	public function the_static_can_decode_reference_ids() {
		list($mediaId, $resourceId) = Reference::decodeReferenceID('23-24');

		$this->assertEquals(23, $mediaId);
		$this->assertEquals(24, $resourceId);
	}

}
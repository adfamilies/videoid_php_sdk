<?php

class TestCase extends PHPUnit_Framework_TestCase 
{

	public function hijackRequest($request, $responses, &$container) 
	{

		$mock = new GuzzleHttp\Handler\MockHandler($responses);
		$history = GuzzleHttp\Middleware::history($container);

		$request->hijack($mock, $history);
	}
	
	public function assertRequestIsBoomSigned($request)
	{
		$this->assertTrue(! empty($request->getHeader('x-boom-date')));
		$this->assertTrue(! empty($request->getHeader('x-boom-sdk-platform')));
		$this->assertTrue(! empty($request->getHeader('x-boom-content-sha256')));
		$this->assertTrue(! empty($request->getHeader('Authorization')));
	}

	public function assertRequestIsVideoIDSigned($request)
	{
		$this->assertTrue(! empty($request->getHeader('Authorization')));
	}

}

class FakeFile extends \SplFileInfo {

	public function getPathname()
	{
		return '/dev/null';
	}

}
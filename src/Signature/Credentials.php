<?php

namespace VideoID\SDK\Signature;

use VideoID\SDK\Request\Options;

class Credentials
{

    /** @var string API Access Key ID */
    protected $key;

    /** @var string API Secret Access Key */
    protected $secret;

    /** @var string API Security Token */
    protected $token;

    /** @var int Time to die of token */
    protected $ttd;

    public function __construct($accessKeyId, $secretAccessKey, $token = null, $expiration = null)
    {
        $this->key = trim($accessKeyId);
        $this->secret = trim($secretAccessKey);
        $this->token = $token;
        $this->ttd = $expiration;
    }


    public function serialize()
    {
        return json_encode(array(
            Options::KEY       => $this->key,
            Options::SECRET    => $this->secret,
            Options::TOKEN     => $this->token,
            Options::TOKEN_TTD => $this->ttd
        ));
    }

    public function unserialize($serialized)
    {
        $data = json_decode($serialized, true);
        $this->key    = $data[Options::KEY];
        $this->secret = $data[Options::SECRET];
        $this->token  = $data[Options::TOKEN];
        $this->ttd    = $data[Options::TOKEN_TTD];
    }

    /**
     * Returns the BOOM access key ID for this credentials object.
     *
     * @return string
     */
    public function getAccessKeyId()
    {
        return $this->key;
    }

    /**
     * Returns the BOOM secret access key for this credentials object.
     *
     * @return string
     */
    public function getSecretKey()
    {
        return $this->secret;
    }

    /**
     * Get the associated security token if available
     * //TODO: unused
     *
     * @return string|null
     */
    public function getSecurityToken()
    {
        return $this->token;
    }

    /**
     * Get the UNIX timestamp in which the credentials will expire
     *
     * @return int|null
     */
    public function getExpiration()
    {
        return $this->ttd;
    }

    /**
     * Set the BOOM access key ID for this credentials object.
     *
     * @param string $key BOOM access key ID
     *
     * @return self
     */
    public function setAccessKeyId($key)
    {
        $this->key = $key;

        return $this;
    }

    /**
     * Set the BOOM secret access key for this credentials object.
     *
     * @param string $secret BOOM secret access key
     *
     * @return CredentialsInterface
     */
    public function setSecretKey($secret)
    {
        $this->secret = $secret;

        return $this;
    }

    /**
     * Set the security token to use with this credentials object
     *
     * @param string $token Security token
     *
     * @return self
     */
    public function setSecurityToken($token)
    {
        $this->token = $token;

        return $this;
    }

    /**
     * Set the UNIX timestamp in which the credentials will expire
     * //TODO: unused
     *
     * @param int $timestamp UNIX timestamp expiration
     *
     * @return self
     */
    public function setExpiration($timestamp)
    {
        $this->ttd = $timestamp;

        return $this;
    }

    /**
     * Check if the credentials are expired
     *
     * @return bool
     */
    public function isExpired()
    {
        return $this->ttd !== null && time() >= $this->ttd;
    }
}

<?php

namespace VideoID\SDK;

use VideoID\SDK\Exceptions\LimitExceededException;
use VideoID\SDK\Request\CreateMediaRequest;
use VideoID\SDK\Request\CreateResourceRequest;
use VideoID\SDK\Request\TotalDurationRequest;
use VideoID\SDK\Request\VideoIDMatchRequest;
use VideoID\SDK\Request\VideoIDResultRequest;
use VideoID\SDK\Request\ViewMediaRequest;
use VideoID\SDK\Request\ViewResourceRequest;

class VideoIDSDK
{
    const DURATION_LIMIT = 3600000;

    protected $apiKey;
    protected $apiSecret;
    protected $videoIDAuth;

    public function __construct($apiKey, $apiSecret, $videoIDAuth)
    {
        $this->apiKey = $apiKey;
        $this->apiSecret = $apiSecret;
        $this->videoIDAuth = $videoIDAuth;
    }

    public function uploadReference($name, \SplFileInfo $file)
    {
        $referenceID = $this->asyncUploadReference($name, $file);
        $Reference = $this->getReference($referenceID);
        while (! $Reference->isProcessed()) {
            sleep(1);
            $Reference = $this->getReference($referenceID);
        }

        return $referenceID;
    }

    public function asyncUploadReference($name, \SplFileInfo $file)
    {
        $totalDuration = (new TotalDurationRequest($this->apiKey, $this->apiSecret))->send();
        if ($totalDuration > static::DURATION_LIMIT) {
            throw new LimitExceededException("You reached the maximum allowed duration: " . static::DURATION_LIMIT / 60 / 60 . " hours");
        }

        $mediaId = (new CreateMediaRequest($this->apiKey, $this->apiSecret))->send($name);
        $resourceId = (new CreateResourceRequest($this->apiKey, $this->apiSecret))->send($mediaId, $file);
        return Reference::createReferenceID($mediaId, $resourceId);
    }

    public function getReference($referenceID)
    {
        list($mediaId, $resourceId) = Reference::decodeReferenceID($referenceID);
        $media = (new ViewMediaRequest($this->apiKey, $this->apiSecret))->send($mediaId);
        $resource = (new ViewResourceRequest($this->apiKey, $this->apiSecret))->send($mediaId, $resourceId);
        return new Reference($media, $resource);
    }

    public function query(\SplFileInfo $file)
    {
        $queryID = (new VideoIDMatchRequest($this->videoIDAuth))->send($file);
        $result = (new VideoIDResultRequest($this->videoIDAuth))->send($queryID);
        while ($result->status=='PROCESSING') {
            sleep(1);
            $result = (new VideoIDResultRequest($this->videoIDAuth))->send($queryID);
        }

        return $this->processResult($result->segments);
    }

    private function processResult($segments)
    {
        $medias = [];
        $medias[0] = [
            'id' => null,
            'name' => 'No Match',
            'intervals' => []
        ];
        foreach ($segments as $segment) {
            if ($segment->status=="MATCH") {
                $resourceId = $segment->metadata->result->resource;
                $mediaId = $segment->metadata->result->media;
                $mediaName = $segment->metadata->result->media_name;
                $start_second = $segment->metadata->split_start_second;
                $end_second = $segment->metadata->split_end_second;
                if (! array_key_exists($mediaId, $medias)) {
                    $medias[$mediaId] = [
                        'id' => $mediaId . '-' . $resourceId,
                        'name' => $mediaName,
                        'intervals' => []
                    ];
                }
                $medias[$mediaId]['intervals'][] = [$start_second, $end_second];
            } else {
                $start_second = $segment->metadata->split_start_second;
                $end_second = $segment->metadata->split_end_second;
                $medias[0]['intervals'][] = [$start_second, $end_second];
            }
        }

        $totalSegments = count($segments);
        foreach ($medias as $index => $media) {
            $intervals = $this->aggregateIntervals($media['intervals']);
            $medias[$index]['percentage'] = (int) round(count($media['intervals']) / $totalSegments * 100, 0);
            $medias[$index]['intervals'] = $intervals;
        }

        if (count($medias[0]['intervals']) <= 0) {
            unset($medias[0]);
        }

        return $medias;
    }

    private function aggregateIntervals($intervals)
    {
        $aggregatedIntervals = [];
        $aggregatedIndex = 0;
        foreach ($intervals as $interval) {
            if (count($aggregatedIntervals) > 0) {
                if ($aggregatedIntervals[$aggregatedIndex][1]==$interval[0]) {
                    $aggregatedIntervals[$aggregatedIndex][1] = $interval[1];
                } else {
                    $aggregatedIndex++;
                    $aggregatedIntervals[$aggregatedIndex] = $interval;
                }
            } else {
                $aggregatedIntervals[$aggregatedIndex] = $interval;
            }
        }

        return $aggregatedIntervals;
    }
}

<?php

namespace VideoID\SDK\Request;

use VideoID\SDK\Exceptions\InternalErrorException;

class TotalDurationRequest extends BoomAppRequest
{
    
    public function send()
    {
        $httpClient = new \GuzzleHttp\Client(['handler' => $this->stack, 'connect_timeout' => 30, ]);
        $options = [
            'headers' => [
                'x-boom-date'           => gmdate('Ymd\THis\Z', time()),
                'x-boom-sdk-platform'   => 'PHP',
                'x-boom-content-sha256' => 'c633444c758c27f2518c5e27096ffb356790513d6b57792a71ad6a78a4a42c04'
            ],
        ];
        try {
            $response = $httpClient->request('GET', $this->apiBaseUrl . $this->apiVersion .'/app/total-duration', $options);
        } catch (\GuzzleHttp\Exception\RequestException $e) {
            $this->handleRequestException($e);
        } catch (\Exception $e) {
            throw new InternalErrorException($e->getMessage());
        }
        $this->handleResponseException($response);
        $header = 'HTTP/1.1 ' . $response->getStatusCode() . ' ' . $response->getReasonPhrase();
        $headers = $response->getHeaders();
        array_unshift($headers, $header);
        try {
            $body = json_decode($response->getBody());
            return $body;
        } catch (\Exception $e) {
            throw new InternalErrorException($response->getBody());
        }
    }
}

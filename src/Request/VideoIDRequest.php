<?php

namespace VideoID\SDK\Request;

use VideoID\SDK\Exceptions\InternalErrorException;
use VideoID\SDK\Exceptions\InvalidCredentialsException;
use VideoID\SDK\Signature\Credentials;
use VideoID\SDK\Signature\SignatureV1;

abstract class VideoIDRequest
{
    protected $stack;
    protected $auth;

    protected $apiBaseUrl = 'https://video-id-stag.boomapp.co';

    public function __construct($auth)
    {
        $this->auth = $auth;
        $this->stack = new \GuzzleHttp\HandlerStack();
        $this->stack->setHandler(new \GuzzleHttp\Handler\CurlHandler());
    }

    public function hijack($handler, $history) {
        $this->stack->setHandler($handler);
        $this->stack->push($history);
    }

    protected function handleRequestException(\GuzzleHttp\Exception\RequestException $e)
    {
        if ($e->hasResponse()) {
            if ($e->getResponse()->getStatusCode()==403) {
                throw new InvalidCredentialsException($e->getResponse()->getReasonPhrase());
            }
        }
            
        throw new InternalErrorException();
    }

    protected function handleResponseException($response)
    {
        $body = json_decode($response->getBody());

        if ($response->getStatusCode()!=200) {
            if ($response->getStatusCode()==403) {
                throw new InvalidCredentialsException();
            }

            throw new InternalErrorException($response->getReasonPhrase());
        }

        if (is_null($body)) {
            throw new InternalErrorException("Invalid json response");
        }
    }
}

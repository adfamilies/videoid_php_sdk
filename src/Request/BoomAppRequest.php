<?php

namespace VideoID\SDK\Request;

use VideoID\SDK\Exceptions\InternalErrorException;
use VideoID\SDK\Exceptions\InvalidCredentialsException;
use VideoID\SDK\Signature\Credentials;
use VideoID\SDK\Signature\SignatureV1;

abstract class BoomAppRequest
{
    protected $stack;

    protected $apiBaseUrl = 'https://api-v2-stag.boomapp.co';
    protected $apiVersion = '/v2.0';

    public function __construct($apiKey, $apiSecret)
    {
        $this->stack = new \GuzzleHttp\HandlerStack();
        $this->stack->setHandler(new \GuzzleHttp\Handler\CurlHandler());
        $this->stack->push(\GuzzleHttp\Middleware::mapRequest(function (\Psr\Http\Message\RequestInterface $request) use ($apiKey, $apiSecret) {
            $signature = new SignatureV1('boomapp', 'europe');
            $signature->setConfig('BA1', 'HMAC', 'SHA256', 'x-boom-');
            $signature->setOverrideDateHeaders(false);
            $credentials = new Credentials($apiKey,  $apiSecret);
            $signedHeaders = $signature->signRequest($request, $credentials);

            return new \GuzzleHttp\Psr7\Request(
                $request->getMethod(),
                $request->getUri(),
                $signedHeaders,
                $request->getBody()
            );
        }));
    }

    public function hijack($handler, $history) {
        $this->stack->setHandler($handler);
        $this->stack->push($history);
    }

    protected function handleRequestException(\GuzzleHttp\Exception\RequestException $e)
    {
        if ($e->hasResponse()) {
            if ($e->getResponse()->getStatusCode()==403) {
                throw new InvalidCredentialsException($e->getResponse()->getReasonPhrase());
            }
        }
        
        throw new InternalErrorException();
    }

    protected function handleResponseException($response)
    {
        $body = json_decode($response->getBody());

        if ($response->getStatusCode()!=200) {
            if ($response->getStatusCode()==403 || (!is_null($body) && ($body->error->reason=="invalidApiId" || $body->error->reason=="forbidden"))) {
                throw new InvalidCredentialsException(!is_null($body) ? $body->error->message : $response->getReasonPhrase());
            }

            throw new InternalErrorException($response->getReasonPhrase());
        }

        if (is_null($body)) {
            throw new InternalErrorException("Invalid json response");
        }
    }
}

<?php

namespace VideoID\SDK\Request;

use VideoID\SDK\Utils\Enum;

/**
 * Contains enumerable default factory options that can be passed to a client's factory method
 */
class Options extends Enum
{
    /**
     * API Access Key ID
     */
    const KEY = 'key';

    /**
     * API secret access key
     */
    const SECRET = 'secret';

    /**
     * Custom API security token to use with request authentication.
     */
    const TOKEN = 'token';

    /**
     * @var string UNIX timestamp for when the custom credentials expire
     */
    const TOKEN_TTD = 'token.ttd';

    /**
     * @var string Region name (e.g. 'us-east-1', 'us-west-1', 'us-west-2', 'eu-west-1', etc...)
     */
    const REGION = 'region';

    /**
     * @var string URI Scheme of the base URL (e.g. 'https', 'http').
     */
    const SCHEME = 'scheme';

    /**
     * @var string Specify the name of the service
     */
    const SERVICE = 'service';

    /**
     * @var string Specify the short date of the service
     */
    const SCOPE_DATE = 'scope.date';

    /**
     * @var string Specify the scope version (request tag) of the service
     */
    const SCOPE_VERSION = 'scope.version';
}

<?php

namespace VideoID\SDK\Request;

use VideoID\SDK\Exceptions\InternalErrorException;
use VideoID\SDK\Exceptions\InvalidCredentialsException;
use VideoID\SDK\Signature\Credentials;
use VideoID\SDK\Signature\SignatureV1;

class VideoIDMatchRequest extends VideoIDRequest
{
    
    public function send($file)
    {
        $httpClient = new \GuzzleHttp\Client(['handler' => $this->stack, 'connect_timeout' => 30, ]);
        $options = [
            'headers' => [
                'Authorization' => 'Basic ' . $this->auth,
            ],
            'multipart' => [
                [
                    'name' => 'matching_file',
                    'contents' => fopen($file->getPathname(), 'r'),
                ]
            ],
        ];
        try {
            $response = $httpClient->request('POST', $this->apiBaseUrl .'/matching', $options);
        } catch (\GuzzleHttp\Exception\RequestException $e) {
            $this->handleRequestException($e);
        } catch (\Exception $e) {
            throw new InternalErrorException($e->getMessage());
        }
        $this->handleResponseException($response);
        $header = 'HTTP/1.1 ' . $response->getStatusCode() . ' ' . $response->getReasonPhrase();
        $headers = $response->getHeaders();
        array_unshift($headers, $header);
        try {
            $body = json_decode($response->getBody());
            return $body->id;
        } catch (\Exception $e) {
            throw new InternalErrorException($response->getBody());
        }
    }

}

<?php

namespace VideoID\SDK\Request;

use VideoID\SDK\Exceptions\InternalErrorException;
use VideoID\SDK\Exceptions\InvalidCredentialsException;
use VideoID\SDK\Signature\Credentials;
use VideoID\SDK\Signature\SignatureV1;

class VideoIDResultRequest extends VideoIDRequest
{

    public function send($queryID)
    {
        $httpClient = new \GuzzleHttp\Client(['handler' => $this->stack, 'connect_timeout' => 30, ]);
        $options = [
            'headers' => [
                'Authorization' => 'Basic ' . $this->auth,
            ]
        ];
        try {
            $response = $httpClient->request('GET', $this->apiBaseUrl .'/matchings?process_id='.$queryID, $options);
        } catch (\GuzzleHttp\Exception\RequestException $e) {
            $this->handleRequestException($e);
        } catch (\Exception $e) {
            throw new InternalErrorException($e->getMessage());
        }
        $this->handleResponseException($response);
        $header = 'HTTP/1.1 ' . $response->getStatusCode() . ' ' . $response->getReasonPhrase();
        $headers = $response->getHeaders();
        array_unshift($headers, $header);
        try {
            $body = json_decode($response->getBody());
            return $body;
        } catch (\Exception $e) {
            throw new InternalErrorException($response->getBody());
        }
    }
    
}

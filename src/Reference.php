<?php

namespace VideoID\SDK;

class Reference
{
    private $mediaState;
    private $resourceState;

    public function __construct($mediaState, $resourceState)
    {
        $this->mediaState = $mediaState;
        $this->resourceState = $resourceState;
    }

    public function getID()
    {
        return static::createReferenceID($this->mediaState->id, $this->resourceState->id);
    }

    public function getName()
    {
        return $this->mediaState->name;
    }

    public function getDuration()
    {
        return $this->resourceState->duration;
    }

    public function isProcessed()
    {
        return $this->resourceState->process_status == 'DATA_SAVED';
    }

    public static function createReferenceID($mediaId, $resourceId)
    {
        return $mediaId . "-" . $resourceId;
    }

    public static function decodeReferenceID($referenceID)
    {
        return explode('-', $referenceID);
    }
}
